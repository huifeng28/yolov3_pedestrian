项目整体打包已放在[百度云](https://pan.baidu.com/s/1NTGojrVxy02ulVW2TKfw4g?pwd=dmrs)中

---
# 1、环境配置
## 1.1 ubantu
本项目是基于ubantu20.04，主要由于在训练过程中 Linux 可以更好的调动硬件性能。
## 1.2 GPU环境搭建
以我的笔记本电脑为例，CPU 是 Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz，GPU 是 GeForce GTX 1060。然后我分别用 CPU 和 GPU 进行 yolov3 训练相同的一组数据，时长对比如下图。CPU 训练时长在 220s，GPU 在1s 左右，差距在 220 倍。由此可见，GPU 环境搭建的必要性。
![输入图片说明](png/%E5%9B%BE%E7%89%871.png)
### 1.2.1 安装NVDIA显卡驱动
首先我们需要禁用 Ubuntu 自带的驱动 nouveau，具体操作如下：
打开系统的 blacklist。
```c
sudo vim /etc/modprobe.d/blacklist-nouveau.conf
```
在 blacklist 中添加禁用 nouveau 的语句。
```c
blacklist nouveau
options nouveau modeset=0
```
更新 initramfs。
```c
sudo update-initramfs -u
```
重启系统。
```c
sudo reboot
```
然后我们查找系统推荐的 nvdia 驱动。
```c
ubuntu-drivers devices
```
最后我们安装系统推荐的 nvdia 驱动。
```c
sudo ubuntu-drivers autoinstall
```
之后重启就可以查看 gpu 信息。
![输入图片说明](png/%E5%9B%BE%E7%89%872.png)
### 1.2.2 安装CUDA
我们从[CUDA官网](https://developer.nvidia.cn/cuda-downloads) 下载并安装下图所选。
![输入图片说明](png/%E5%9B%BE%E7%89%873.png)
安装完成后，需要配置环境变量，编辑~/.bashrc 文件，在末尾添加。
```c
export CUDA_HOME=/usr/local/cuda-11.7
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${CUDA_HOME}/lib64
export PATH=${CUDA_HOME}/bin:${PATH}
```
然后查看 cuda 是否安装成功。
![输入图片说明](png/%E5%9B%BE%E7%89%874.png)
### 1.2.3 安装cuDNN
从 cuDNN 官网下载相应的版本（cuDNN Library for Linux (x86_64)）的 tar 压缩包。解压后，执行以下命令，拷贝到相应的文件。
```c
sudo cp cudnn-*-archive/include/cudnn*.h /usr/local/cuda/include
sudo cp -P cudnn-*-archive/lib/libcudnn* /usr/local/cuda/lib64
sudo chmod a+r /usr/local/cuda/include/cudnn*.h /usr/local/cuda/lib64/libcudnn*
```
之后便可以查看 cuDNN。
```c
cat /usr/local/cuda/include/cudnn_version.h | grep CUDNN_MAJOR -A 2
```

至此，GPU 环境搭建完成。
## 1.3 项目环境搭建
首先在我们的 Ubuntu 系统上，使用 git 命令直接克隆 YOLOv3 的源码工程：
```c
git clone https://github.com/pjreddie/darknet
```
YOLOv3 使用的是开源的神经网络框架 Darknet53，有 CPU 和 GPU 两种模式。默认使用的是 CPU模式，然后我们修改 darknet 目录下的 Makefile 文件，将 CPU 模式改为 GPU 模式。把 GPU 和 CUDNN 都改为 1。
```c
GPU=1
CUDNN=1
OPENCV=0
OPENMP=0
DEBUG=0
```
从 NVDIA 官网查找自己的 GPU 算力，比如我的 GTX1060 算力为 6.1。
```c
# This is what I use, uncomment if you know your arch and want to specify
ARCH= -gencode arch=compute_61,code=compute_61
```
然后修改 NVCC 的路径。
```c
NVCC=/usr/local/cuda-11.7/bin/nvcc
```
修改完后，在 darknet 目录下执行 make。
至此，项目环境已经搭建完成。
## 1.4 环境测试
下载官方提供的训练好的权重文件。
```c
wget https://pjreddie.com/media/files/yolov3.weights
```
测试识别对象。
```c
./darknet detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights data/person.jpg
```
在上述命令中，detect 表示调用 darknet 的对象识别功能，coco.data 是记录配置文件位置 yolov3.cfg 是配置文件，包含各层网络的参数、要识别的物体种类数等，yolo.weights 是上一步下载的权重数据，data/person.jpg 是要进行对象识别的照片。

命令执行完成后，将会把识别结果生成到 predictions.jpg。打开图片如下图所示：
![输入图片说明](png/%E5%9B%BE%E7%89%875.png)
# 2、使用说明
## 2.1 检测
在”yolov3”目录下打开终端运行。
### 2.1.1 视频检测
```python
python3 yolov3_test_person.py --video=test/test.flv --device 'cpu'
python3 yolov3_test_person.py --video=test/test.flv --device 'gpu'
```
效果如图所示
![输入图片说明](png/%E5%9B%BE%E7%89%877.png)

### 2.1.2 图片检测
```python
python3 yolov3_test_person.py --image=test/test.jpg --device 'cpu'
python3 yolov3_test_person.py --image=test/test.jpg --device 'gpu'
```
效果如图所示
![输入图片说明](png/%E5%9B%BE%E7%89%876.png)

### 2.1.3 摄像头检测
```python
python3 yolov3_test_person.py --device 'cpu'
python3 yolov3_test_person.py --device 'gpu'
```
其中yolov3_test_person.py可以替换成yolov3.py，是使用官方权重进行测试。yolov3_test_person.py默认使用的是person_32_2000.weights，如果想使用person_16_10000.weights只需修改modelWeights 即可。
`注：图片或视频保存在输入的目录，而不是save，比如上述文件就是保存在test文件夹，摄像头视频保存在yolov3文件夹下。`

## 2.2 训练
训练时，默认环境是GPU=1，cuDNN=1，直接在目录“yolov3/darknet”下，运行以下命令，最后文件保存在“yolov3/darknet/backup”目录下。
```c
make
./darknet detector train cfg/person.data cfg/person.cfg darknet53.conv.74 -gpus 0>&1 > tiny.log
```
注：如果想要自己制作训练集训练，可以参考以下博客
[https://redstonewill.blog.csdn.net/article/details/117425017](https://redstonewill.blog.csdn.net/article/details/117425017)

## 2.3 可视化
可视化采用了loss 和 IOU 图。
可视化时，将tiny.log拷贝到“yolov3/darknet/visualization”目录下，然后运行visualization_loss.py。
```c
python3 visualization_loss.py
```
可视化效果如下图所示
![输入图片说明](png/%E5%9B%BE%E7%89%878.png)
![输入图片说明](png/%E5%9B%BE%E7%89%879.png)
